# PaintByNumbers

Paint by numbers / Pixel painter discord bot for the Pimoroni GFX Hat (128x64 RGB Backlight LCD)
![Example](images/example.png "Example")

# Using my bot (if it is up)
In the `#paint-by-numbers` [discord channel](https://discord.gg/cD3Xb6H) 
## Pixels
`.setpixel <x> <y> <state>`
```
x: 0 - 127
y: 0 - 63
state: 0/1 (off/on)
```
## Backlight
`.setbacklight <r> <g> <b>`
```
Literally basic rgb lmao
r,g,b: 50 - 164
```
# If you want to setup this yourself 
You will need the [GFXHat](https://shop.pimoroni.com/products/gfx-hat) and a RPi3 (RPi4 Untested).

Install discord.py and the [GFXHat Library](https://github.com/pimoroni/gfx-hat/)

Move the config.py.example to config.py and put contents as 
`token = "bot token here"`

Realize the mistake you made.


