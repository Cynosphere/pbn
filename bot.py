#!/usr/bin/env python
import config

from gfxhat import lcd, backlight

import discord
import asyncio
from discord.ext import commands

from quart import Quart
from quart import request
from logging import Formatter, getLogger
from quart.logging import serving_handler, default_handler
from hypercorn.asyncio.run import Server
from hypercorn.config import Config

backlight.setup()
backlight.set_all(50, 50, 150)
backlight.show()
print("pretend it backlight")

# REST API here
app = Quart(__name__)

serving_handler.setFormatter(Formatter('%(message)s'))

@app.route("/")
@app.errorhandler(404)
async def rest_root():
    return "nothing is here"

@app.route("/setpixel")
async def rest_setpixel():
    x = request.args.get("x")
    y = request.args.get("y")
    state = request.args.get("state")

    if not x:
        return "no x value given", 400
    if not y:
        return "no y value given", 400
    if not state:
        state = 1

    x = max(0, min(int(x), 127))
    y = max(0, min(int(y), 63))
    state = max(0, min(int(state), 1))

    lcd.set_pixel(x, y, state)
    lcd.show()
    # print("rest: set pixel {0} {1} {2}".format(x, y, state))

    # return "set pixel {0} {1} {2}".format(x, y, state)
    return ""

@app.route("/setbacklight")
async def rest_setbacklight():
    r = request.args.get("r")
    g = request.args.get("g")
    b = request.args.get("b")

    if not r:
        return "no red", 400
    if not g:
        return "no green", 400
    if not b:
        return "no blue", 400

    r = max(50, min(int(r), 164))
    g = max(50, min(int(g), 164))
    b = max(50, min(int(b), 164))

    backlight.setup()
    backlight.set_all(r, g, b)
    backlight.show()
    # print("rest: set backlight to {0},{1},{2}".format(r, g, b))

    return "set backlight to {0},{1},{2}".format(r, g, b)

# discord bot
hc_config = Config()
hc_config.bind = ["127.0.0.1:{0}".format(config.port)]

async def _hypercorn_bs(loop):
    socket = hc_config.create_sockets()
    server = await loop.create_server(
        lambda: Server(app, loop, hc_config),
        sock=socket.insecure_sockets[0],
    )

    return server

class PBNBot(commands.Bot):
    def __init__(self, command_prefix, help_command=commands.bot._default, description=None, **options):
        super().__init__(command_prefix, help_command, description, **options)

        self.loop.create_task(self._rest_server())

    async def _rest_server(self):
        print("attempting to rest server")
        try:
            self.rest_server = await _hypercorn_bs(self.loop)
            print("wow it worked")
        except Exception:
            print("failed, oh well")

    async def close(self):
        if self.rest_server:
            print("stopping rest server")
            self.rest_server.close()
            await self.rest_server.wait_closed()
        print("stopping bot")
        await super().close()

client = PBNBot(command_prefix=config.prefix)

@client.event
async def on_ready():
    print("Logged in as {0.name}#{0.discriminator}".format(client.user))

# Backlight!
@client.command()
async def setbacklight(ctx, r: int, g: int, b: int):
    """
    Sets backlight color

    min 50, max 164
    """

    r = max(50, min(r, 164))
    g = max(50, min(g, 164))
    b = max(50, min(b, 164))

    backlight.setup()
    backlight.set_all(r, g, b)
    backlight.show()
    print("{0.name}#{0.discriminator} set backlight to {1},{2},{3}".format(ctx.author, r, g, b))

# Pixels!
# lcd.set_pixel(1, 1, 0)
@client.command()
async def setpixel(ctx, x: int, y: int, state: int):
    """
    Sets a pixel

    x: 0 - 127
    y: 0 - 63
    state: 0/1
    """

    x = max(0, min(x, 127))
    y = max(0, min(y, 63))
    state = max(0, min(state, 1))

    lcd.set_pixel(x, y, state)
    lcd.show()
    print("{0.name}#{0.discriminator} set pixel {1} {2} {3}".format(ctx.author, x, y, state))

client.run(config.token)

try:
    signal.pause()
except KeyboardInterrupt:
    for x in range(6):
        backlight.set_pixel(x, 0, 0, 0)
        touch.set_led(x, 0)
    backlight.show()
    lcd.clear()
    lcd.show()